import express from "express"
import cors from "cors"
import morgan from "morgan"

import auth from "./routes/auth.js"
import debug from "./routes/debug.js"
import { generalErrorHandler } from "./middleware/middleware.js"


const app = express()
app.use(cors())
app.use(morgan("dev"))
app.use(express.json())



app.get("/", (_req, res) => res.send("Backend Server is up and running!"));
app.use("/auth", auth)
app.use("/debug", debug)


app.use(generalErrorHandler)

export default app
