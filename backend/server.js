import app from "./app.js";
import mongoose from "mongoose"
import dotenv from 'dotenv'
dotenv.config()


let mongoURI;
switch (process.env.NODE_ENV){
    case "dev":
        mongoURI = process.env.MONGODB_URI_DEV;
        break;
    case "test":
        mongoURI = process.env.MONGODB_URI_TEST
        break;
}

mongoose.connect(mongoURI).then(() => {
  console.log(`⚡[database][${new Date().toLocaleTimeString()}]: remote ${process.env.NODE_ENV.toUpperCase()} database is connected`)
  app.listen(3000, () => {
    console.log(
      `⚡[server][${new Date().toLocaleTimeString()}]:`,
      `running on http://localhost:3000`
    );
  })
}, 
(err) => {
  console.log(process.env.MONGODB_URI)
  console.log("database error")
})
