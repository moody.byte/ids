import jwt from "jsonwebtoken";
import { ReasonPhrases, StatusCodes } from "http-status-codes";

export const tokenRequired = (req, res, next) => {
    new Promise((resolve, reject) => {
        resolve(jwt.verify(req.body.token, process.env.SECRET_KEY, {algorithms:["HS256"]}))
    })
    .then((result) => {
        if (result.alg != "HS256") {
            Promise.reject({
                status:401,
                reason:"bad token"
            })
        }
        console.log(result)
        res.locals.payload = result
        return next() 
    })
    .catch((err) => {
        next(err)
    })
}

export const generalErrorHandler = (err, req, res, next) => {

    console.log(err)
    return res.send({
        status: err.status ? err.status : StatusCodes.INTERNAL_SERVER_ERROR,
        reason: err.reason ? err.reason : ReasonPhrases.INTERNAL_SERVER_ERROR,
        note: "see server logs for further information"
    })
}
