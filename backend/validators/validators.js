
export const RegisterRequestValidator = (body) => {
    return body.hasOwnProperty('email') && body.hasOwnProperty('password') && body.hasOwnProperty('role');
};

export const LoginRequestValidator = (body) => {
    return body.hasOwnProperty("email") && body.hasOwnProperty("password")
};