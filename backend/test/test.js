import app from "../app.js"
import mongoose from "mongoose";
import dotenv from 'dotenv'
dotenv.config()

beforeAll(() => {
  return mongoose.connect(process.env.MONGODB_URI_TEST, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
});

afterAll(() => {
  return mongoose.connection.db.dropDatabase(() => {
    mongoose.connection.close();
  });
});

it("Testing to see if Jest works", () => {
  expect(1).toBe(1);
});
