## All implemented backend routes in HelloResearch V2

### GET /
The base route for the backend. Should display a simple string stating that the 
service is up and running

### GET /debug
A dummy route that contains whatever experimental new features are being 
implemented

### GET /auth
The base route for all web token related functions. Should display a simple 
string that the service is up and running  
TODO: setup a captcha system to prevent abuse

### POST /auth/register
Makes a new user in the database  
TODO: implement a captcha verification to prevent spam and/or whitelist only the 
frontend IP with CORS
Expected post body:
```
{
    "username":string,
    "password":string
}
```
Expected Response - 201:  
```
{
    message:"User created",
    email:string
}
```

### POST /auth/login
Returns a refresh token if the credentials are correct  
Expected post body:
```
{
    "username":string,
    "password":string
}
```
Expected Response - 200:  
```
{
    type:"refresh",
    token:strong
}
```
