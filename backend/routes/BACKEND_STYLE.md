## Backend Style Guide

This document is a guide for the style and techniques that are used in the backend
routes.

### Promisify Everything

This rule may actually change and some routes may be rewritten to use await
instead, until then use the following paradigm.

Wrap everything into a Promise and keep promise chaining for every subsequent
promise. This is done by returning a Promise at the end of each Promise, of which
the result becomes the parameter for the next Promise.

Anytime that you want to send an error code as the response, use `return Promise.reject()` 
with the parameter of a JSON with keys 'status' and 'reason'. This all then funnels
into the final .catch() at the end of the promise chain, and sends the error to the
error handling middleware. The only exception to this rule is if there is a custom
constructed Promise object, at which use the default `reject()` 

The error handling middleware then differentiates if the error is an intended 
response, or if it's an unexpected error. If the error is unexpected, it sanitizes 
the response into a 500 internal server error, thus preventing an attacker from 
seeing any unintended data. Otherwise, return the desired error response.

All desired middleware that you want to execute before the function should be 
passed in as an array before the route callback.

The code snippet highlights these specifications. However note that is a nonsensical
example that serves only to highlight style. 

```javascript
// User.findOne returns a promise, of which the result gets passed into the first
// .then() into the parameter data
route.post(
    "/testing",
    [tokenRequired, moodLogger],  // middleware to execute before the body of this route here
    (req, res, next) => {         // make sure the next parameter is there
        new Promise((resolve, reject) => {
            if (!RequestValidator(req.body)) {    // this custom Promise object
                reject({                          // should use the defined reject()
                    status:StatusCodes.BAD_REQUEST,
                    reason:ReasonPhrases.BAD_REQUEST
                })
            }
            resolve();
        })
        .then(() => {
            return User.findOne({ email: email })  // a more typical promise, return
        })                                         // the promise itself to Promise chain
        .then((data) => {
            if (data == null) {
                return Promise.reject({
                    status: StatusCodes.FORBIDDEN,
                    message: "email does not exist",
                });
            }
            return bcrypt.compare(password, data.hashed_password);
        })
        // the result of bcrypt.compare gets fed into the next .then() as the parameter result
        .then((result) => {
            // etc, etc, etc
        })
        .catch((err) => {     // this catch statement stays constant among all the routes
            // pass whatever error that may occur to the error handling middleware  
            next(err, res, next);
        });
    }
);
```
