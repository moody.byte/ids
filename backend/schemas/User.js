import mongoose from "mongoose"

// TODO: Add name, university, date of birth, etc
// implement some framework for infinitely extensible userprofile data?

var UserSchema = new mongoose.Schema({
    email:{
        type:String,
        required:[true, "Email required"]
    },
    hashed_password:{
        type:String,
        required:[true, "password required"]
    },
    role:{
        type:String,
        required:true,
        enum:["student", "professor"]
    }
})



var User = mongoose.model('User', UserSchema );

export default User